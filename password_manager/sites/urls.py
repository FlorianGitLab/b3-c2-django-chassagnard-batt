from django.urls import path, include
from . import views
from .models import Site

sites = Site.objects.all()

urlpatterns = [
    path('', views.site_list, name="site_list"),
    path('edit/<int:pk>/', views.site_edit, name="site_edit"),
    path('delete/', views.site_delete, name="site_delete"),
    path('add/', views.site_add, name="site_add"),
    path('export-as-csv/', views.export_as_csv, name='export_as_csv')
]