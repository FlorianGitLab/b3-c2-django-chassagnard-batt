from django import forms
from .models import Site

class SiteForm(forms.ModelForm):
    class Meta:
        model = Site
        fields = ['name', 'url', 'user', 'pswd']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Password'})
        self.fields['url'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Password'})
        self.fields['user'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Password'})
        self.fields['pswd'].widget.attrs.update({'class': 'form-control', 'type': 'password', 'placeholder': 'Mot de passe'})
