from django.db import models

class Site(models.Model):

    name = models.CharField(max_length=50)
    url = models.CharField(max_length=100)
    user = models.CharField(max_length=50)
    pswd = models.CharField(max_length=50)

    def __str__(self):
        return self.name