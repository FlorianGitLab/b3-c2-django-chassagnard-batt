from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from .models import Site
from .forms import SiteForm
import csv
from django.http import HttpResponse

@login_required
def site_list(request):
	sites = Site.objects.all()
	return render(request, 'site_list.html', {'sites' : sites})

@login_required
def site_add(request):
    if request.method == 'POST':
        form = SiteForm(request.POST)
        form.save()
        return redirect('site_list')
    else:
        form =  SiteForm()
        return render(request,'site_form.html', {'form': form})

@login_required
def site_edit(request, pk=None):
    site = get_object_or_404(Site, pk=pk)
    if request.method == 'POST':
        form = SiteForm(request.POST, instance=site)
        if form.is_valid():
            form.save()
            return redirect('site_list') 
    else:
        context = {'form': SiteForm(instance=site), 'site': site}
        return render(request, 'site_form.html', context)

@login_required
def site_delete(request):
    instance_id = request.GET.get('id')
    Site.objects.filter(pk=instance_id).delete()
    return redirect('site_list')

@login_required
def export_as_csv(request):
    response = HttpResponse(
        content_type='text/csv',
        headers={"Content-Disposition": 'attachment; filename="passwords_export.csv"'},
    )

    writer = csv.writer(response)
    writer.writerow(['Name', 'Url', 'User', 'Password'])

    passwordset = Site.objects.all()
    for password in passwordset:
        writer.writerow([password.name, password.url, password.user, password.pswd])

    return response