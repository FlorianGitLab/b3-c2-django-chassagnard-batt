# b3-c2-django-chassagnard-batt

## PASSWORD MANAGER

### Auteurs :
BATT Florian \
CHASSAGNARD Léa

***

### Mode d'emploi :

Pour se connecter au Password Manager, entrer comme utilisateur et mot de passe : `admin` `admin`. \
Les fonctionnalités qui seront ensuite accessibles sont détaillées dans les deux listes ci-dessous.

***

### Fonctionnalités de Base :
- Liste des sites (page `/sites/`)
- Ajout de sites (bouton `+ Ajouter un site` sur la page de la liste)
- Modification de site (bouton `Editer` pour un site dans la liste)
- Suppression de site (bouton `Supprimer` pour un site dans la liste)
- Interface utilisateur, avec Bootstrap, et un favicon

***

### Fonctionnalités Bonus :

- Masquage par défaut du mot de passe :
  - Pour chaque site dans la liste, et aussi sur les pages d'ajout et de modification d'un site
  - Le bouton `Afficher` permet d'afficher le mot de passe, et le bouton `Masquer` qui le remplace permet de le masquer à nouveau
- Login lors de la première ouverture du Password Manager :
  - Il n'existe qu'un utilisateur : `admin`, mot de passe `admin`
- Logout (bouton `Logout` sur la page de la liste), avec redirection vers une page de logout et un lien vers le login
- Export au format CSV (bouton `Télécharger CSV` sur la page de la liste)
  - (puisque nous ne sommes que deux dans le groupe, c'est une fonctionnalité bonus)

***

### Ressenti :
Au début, nous avons trouvé le langage Django plutôt difficile à appréhender, tout comme la méthodologie GitFlow. Mais ce projet nous a permis de bien nous familiariser avec les deux. \
Nous avons aussi remarqué que l'emploi du GitFlow a facilité le travail en équipe, car la méthodologie impose une segmentation nette des différentes fonctionnalités du projet, avec un historique de commits clairs et commentés. \
De cette manière, nous avons pu minimiser les conflits, et faciliter la gestion de ceux qui sont apparus.
